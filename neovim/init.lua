-- Set global variables
local set = vim.opt
  -- Tab stuff
set.tabstop = 2
set.shiftwidth = 2
set.softtabstop = 2
set.expandtab = true

  -- cursor and line highlight stuff
set.number = true
set.relativenumber = true
set.cursorline = true

-- PLUGINS
-- install lazy plugin manager if it is not installed already
local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
  vim.fn.system({
    "git",
    "clone",
    "--filter=blob:none",
    "https://github.com/folke/lazy.nvim.git",
    "--branch=stable", -- latest stable release
    lazypath,
  })
end
vim.opt.rtp:prepend(lazypath)

-- list of plugins to set up and use
require("lazy").setup({
	{"neovim/nvim-lspconfig"},
  -- Dashboard
	{"nvimdev/dashboard-nvim",
		event = 'VimEnter',
		config = function()
			require('dashboard').setup{
				-- dashboard config
				theme = 'hyper',
				config = {
					week_header = {
						enable = true
					},
					shortcut = {
						-- A shortcut to update all plugins via lazy
						{ desc = '🔁 Update', group = '@property', action = 'Lazy update', key = 'u' },
					}
				}
			}
		end,
		dependencies = { { 'nvim-tree/nvim-web-devicons'}}
	},
  -- LuaLine
	{"nvim-lualine/lualine.nvim",
		config = function()
			require('lualine').setup{
				options = {
					-- Lualine config stuff here
					theme = 'auto',
				}
			}
		end,
		dependencies = { 'nvim-tree/nvim-web-devicons' }
	},
  -- TreeSitter
  {"nvim-treesitter/nvim-treesitter",
    build = ":TSUpdate",
    config = function()
      local configs = require("nvim-treesitter.configs")

      configs.setup({
        ensure_installed = { "c", "lua", "vim", "vimdoc", "javascript", "typescript", "html", "css", "scss", "vue"},
        highlight = { enable = true },
        indent = { enable = true }
      })
    end
  },
  -- DelimitMate // Adds matching brackets and quotes and stuff automatically
  {"Raimondi/delimitMate"},
  -- FuGITive
  {"tpope/vim-fugitive"},
  -- Telescope
  {"nvim-telescope/telescope.nvim",
    branch = "0.1.x",
    dependencies = { "nvim-lua/plenary.nvim" }
  },
  -- Color Scheme Sonokai (with TreeSitter support!)
  {"sainnhe/sonokai"},
})

-- activate lspconfig with volar
require("lspconfig").volar.setup({})

-- Setting Color Scheme settings AFTER plugins are enabled,
-- so Sonokai scheme can be found by neovim
set.termguicolors = true
vim.cmd 'colorscheme sonokai'

